<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Equipe;
Use AppBundle\Entity\Epreuve;

/**
 * Classement
 *
 * @ORM\Table(name="classement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassementRepository")
 */
class Classement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="place", type="integer")
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="pts", type="integer")
     */
    private $pts;

    /**
     *
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Equipe", cascade={"persist"}, inversedBy="classement")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $equipe;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Epreuve", cascade={"persist"}, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $epreuve;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return Classement
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set pts
     *
     * @param integer $pts
     * @return Classement
     */
    public function setPts($pts)
    {
        $this->pts = $pts;

        return $this;
    }

    /**
     * Get pts
     *
     * @return integer 
     */
    public function getPts()
    {
        return $this->pts;
    }

    /**
     * @return mixed
     */
    public function getEquipe()
    {
        return $this->equipe;
    }

    /**
     * @param mixed $equipe
     */
    public function setEquipe($equipe)
    {
        $this->equipe = $equipe;
    }

    /**
     * @return mixed
     */
    public function getEpreuve()
    {
        return $this->epreuve;
    }

    /**
     * @param mixed $epreuve
     */
    public function setEpreuve($epreuve)
    {
        $this->epreuve = $epreuve;
    }


}
