<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipe
 *
 * @ORM\Table(name="equipe")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EquipeRepository")
 */
class Equipe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
    * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Epreuve", cascade ={"persist"}, mappedBy="equipes")
    * @ORM\JoinColumn(nullable=true)
    */
    private $epreuves;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Classement", cascade={"persist","remove"}, mappedBy="equipe")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $classement;


    public function __construct()
    {
        $this->equipes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Equipe
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add epreuve
     *
     * @param \AppBundle\Entity\Epreuve $epreuve
     *
     * @return Equipe
     */
    public function addEpreuve(\AppBundle\Entity\Epreuve $epreuve)
    {
        $this->epreuves[] = $epreuve;

        return $this;
    }

    /**
     * Remove epreuve
     *
     * @param \AppBundle\Entity\Epreuve $epreuve
     */
    public function removeEpreuve(\AppBundle\Entity\Epreuve $epreuve)
    {
//        $this->epreuves->removeElement($epreuve);
    }

    /**
     * Get epreuves
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEpreuves()
    {
        return $this->epreuves;
    }

    /**
     * @return mixed
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * @param mixed $classement
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;
    }

}
