<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Epreuve
 *
 * @ORM\Table(name="epreuve")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EpreuveRepository")
 */
class Epreuve
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @Orm\ManytoMany(targetEntity="\AppBundle\Entity\Equipe", inversedBy="epreuves")
     * @ORM\JoinTable(name="equipe_epreuve")
     */
    private $equipes;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Classement", cascade={"persist","remove"}, mappedBy="equipe")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $classement;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->equipes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Epreuve
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     *
     * @return Epreuve
     */
    public function addEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipes[] = $equipe;

        return $this;
    }

    /**
     * Remove equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     */
    public function removeEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipes->removeElement($equipe);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipes()
    {
        return $this->equipes;
    }

    /**
     * @return mixed
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * @param mixed $classement
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;
    }

}
