<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Epreuve;
use AppBundle\Entity\Equipe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Classement;
use AppBundle\Form\ClassementType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Classement controller.
 *
 */
class ClassementController extends Controller
{
    /**
     * Lists all Classement entities.
     *
     * @Route("/list", name="classement_index")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $classements = $em->getRepository('AppBundle:Classement')->findAll();

        return $this->render('classement/index.html.twig', array(
            'classements' => $classements,
        ));
    }

    /**
     * Creates a new Classement entity.
     *
     * @Route("/new/{epreuve}/{equipe}", name="classement_new")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction(Request $request, Epreuve $epreuve, Equipe $equipe)
    {
        $classement = new Classement();
        $form = $this->createForm('AppBundle\Form\ClassementType', $classement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $classement->setEquipe($equipe);
            $classement->setEpreuve($epreuve);
            $em->persist($classement);
            $em->flush();

            return $this->redirectToRoute('epreuve_show', array('id' => $epreuve->getId()));
        }

        return $this->render('classement/new.html.twig', array(
            'classement' => $classement,
            'equipe' => $equipe,
            'epreuve' => $epreuve,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Classement entity.
     *
     * @Route("/view/{id}", name="classement_show")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction(Classement $classement)
    {
        $deleteForm = $this->createDeleteForm($classement);

        return $this->render('classement/show.html.twig', array(
            'classement' => $classement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Classement entity.
     *
     * @Route("/{id}/edit", name="classement_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Classement $classement
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Secure(roles="ROLE_ADMIN")
     */

    public function editAction(Request $request, Classement $classement)
    {
        $deleteForm = $this->createDeleteForm($classement);
        $editForm = $this->createForm('AppBundle\Form\ClassementType', $classement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classement);
            $em->flush();

            return $this->redirectToRoute('epreuve_show', array('id' => $classement->getEpreuve()->getId()));
        }

        return $this->render('classement/edit.html.twig', array(
            'classement' => $classement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Classement entity.
     *
     * @Route("/{id}", name="classement_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, Classement $classement)
    {
        $form = $this->createDeleteForm($classement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($classement);
            $em->flush();
        }

        return $this->redirectToRoute('classement_index');
    }

    /**
     * Lists all Classement entities.
     *
     * @Route("/", name="classement_final")
     * @Method("GET")
     */
    public function finalAction()
    {
        $em = $this->getDoctrine()->getManager();
        $classements = $em->getRepository('AppBundle:Classement')->getClassement();

        return $this->render('classement/final.html.twig', array(
            'classements' => $classements,
        ));
    }

    /**
     * Creates a form to delete a Classement entity.
     *
     * @param Classement $classement The Classement entity
     *
     * @return \Symfony\Component\Form\Form The form
     * @Secure(roles="ROLE_ADMIN")
     */
    private function createDeleteForm(Classement $classement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('classement_delete', array('id' => $classement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
