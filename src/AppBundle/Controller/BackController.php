<?php
/**
 * Created by PhpStorm.
 * User: atruillot
 * Date: 24/01/2017
 * Time: 09:20
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * User controller.
 *
 * @Route("/back")
 */
class BackController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="back_index")

     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('back/index.html.twig', array(
        ));
    }
}